import logo from './logo.jpg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>
          Mental Health Tracker
        </h1>
          <p>
          Look at me go, Samantha Post
        </p>
      </header>
    </div>
  );
}

export default App;
