import "./Navigation.css"

export default function Navigation() {
    return (
        <div id="nav">
            <ul>
                <li>Data</li>
                <li>Journal</li>
                <li>Activities</li>
                <li>Settings</li>
            </ul>
        </div>
    )
}